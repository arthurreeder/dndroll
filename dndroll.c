#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char* argv[]) {
  // Check if a dice type was specified
  if (argc < 2) {
    printf("Usage: dndroll [d4|d6|d8|d10|d12|d20]\n");
    return 1;
  }

  // Seed the random number generator with the current time
  srand(time(0));

  // Determine the number of sides on the dice
  int sides;
  if (sscanf(argv[1], "d%d", &sides) != 1) {
    printf("Invalid dice type specified.\n");
    return 1;
  }

  // Generate the random roll
  int roll = rand() % sides + 1;

  // Print the result
  if (roll == 1) {
    printf("You rolled a 1, Failure!\n");
  } else if (roll == 20) {
    printf("You rolled a 20, Critical Hit!\n");
  } else {
    printf("You rolled a %d\n", roll);
  }

  return 0;
}
